"""
https://github.com/EnergieID/entsoe-py
"""


API_KEY = 'a7568616-f6e3-4baf-b766-679b75d60d4e'

#  Mappings: https://github.com/EnergieID/entsoe-py/blob/master/entsoe/mappings.py

from entsoe import EntsoePandasClient
import pandas as pd

FOLDER = 'D:/amp/europe/'


client = EntsoePandasClient(api_key=API_KEY)
TZ = 'WET'

start = pd.Timestamp('20190101', tz=TZ)
end = pd.Timestamp('20220401', tz=TZ)


da = client.query_day_ahead_prices('GB', start=start, end=end)
da.index = da.index.tz_localize(None)
da.to_csv(FOLDER + 'entsoe_da_prices.csv')

gen = client.query_generation('GB', start=start,end=end, psr_type=None)
gen.index = gen.index.tz_localize(None)
gen.to_csv(FOLDER + 'entsoe_gen.csv')


load = client.query_load('GB', start=start,end=end)


netload = load - gen[['Solar','Wind Offshore','Wind Onshore']].sum(1)



"""
    GB =            '10YGB----------A', 'National Grid BZ / CA/ MBA',                   'Europe/London',
    GB_IFA =        '10Y1001C--00098F', 'GB(IFA) BZN',                                  'Europe/London',
    GB_IFA2 =       '17Y0000009369493', 'GB(IFA2) BZ',                                  'Europe/London',
    GB_ELECLINK =   '11Y0-0000-0265-K', 'GB(ElecLink) BZN',                             'Europe/London',
    UK =            '10Y1001A1001A92E', 'United Kingdom',                               'Europe/London',
"""




"""
country_code = 'BE'  # Belgium
country_code_from = 'FR'  # France
country_code_to = 'DE_LU' # Germany-Luxembourg
type_marketagreement_type = 'A01'
"""

# methods that return Pandas Series

country_codes = ['NO_'+str(i) for i in [1,2,3,4,5]] + ['DK_'+str(i) for i in [1,2]] + ['SE_'+str(i) for i in [1,2,3,4]]
df = {}
for c in country_codes:
	print(c)
	%time ts = client.query_day_ahead_prices(c, start=start, end=end)
	df[c] = ts




"""
client.query_net_position_dayahead(country_code, start=start, end=end)
client.query_crossborder_flows(country_code_from, country_code_to, start, end)
client.query_scheduled_exchanges(country_code_from, country_code_to, start, end, dayahead=False)
client.query_net_transfer_capacity_dayahead(country_code_from, country_code_to, start, end)
client.query_net_transfer_capacity_weekahead(country_code_from, country_code_to, start, end)
client.query_net_transfer_capacity_monthahead(country_code_from, country_code_to, start, end)
client.query_net_transfer_capacity_yearahead(country_code_from, country_code_to, start, end)
client.query_intraday_offered_capacity(country_code_from, country_code_to, start, end,implicit=True)

# methods that return Pandas DataFrames
client.query_load(country_code, start=start,end=end)
client.query_load_forecast(country_code, start=start,end=end)
client.query_load_and_forecast(country_code, start=start, end=end)
client.query_generation_forecast(country_code, start=start,end=end)
client.query_wind_and_solar_forecast(country_code, start=start,end=end, psr_type=None)
client.query_generation(country_code, start=start,end=end, psr_type=None)
client.query_generation_per_plant(country_code, start=start,end=end, psr_type=None)
client.query_installed_generation_capacity(country_code, start=start,end=end, psr_type=None)
client.query_installed_generation_capacity_per_unit(country_code, start=start,end=end, psr_type=None)
client.query_imbalance_prices(country_code, start=start,end=end, psr_type=None)
client.query_contracted_reserve_prices(country_code, start, end, type_marketagreement_type, psr_type=None)
client.query_contracted_reserve_amount(country_code, start, end, type_marketagreement_type, psr_type=None)
client.query_unavailability_of_generation_units(country_code, start=start,end=end, docstatus=None, periodstartupdate=None, periodendupdate=None)
client.query_unavailability_of_production_units(country_code, start, end, docstatus=None, periodstartupdate=None, periodendupdate=None)
client.query_unavailability_transmission(country_code_from, country_code_to, start, end, docstatus=None, periodstartupdate=None, periodendupdate=None)
client.query_withdrawn_unavailability_of_generation_units(country_code, start, end)
client.query_import(country_code, start, end)
client.query_generation_import(country_code, start, end)
client.query_procured_balancing_capacity(country_code, start, end, process_type, type_marketagreement_type=None)
"""
