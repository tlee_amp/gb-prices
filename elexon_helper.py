import pandas as pd
from ElexonDataPortal import api

def f(d, code):
	client = api.Client('ifkwpd4bwpopcle')
	print(d)
	df = client.get_B1610(d, (pd.to_datetime(d) + pd.Timedelta('1 day')).strftime('%Y-%m-%d'), code)
	if len(df) > 0:
		if 'local_datetime' in df.columns:
			return df.set_index('local_datetime')['quantity']

def dem(d):
	client = api.Client('ifkwpd4bwpopcle')
	print(d)
	df = client.get_SYSDEM(d, (pd.to_datetime(d) + pd.Timedelta('1 day')).strftime('%Y-%m-%d'))
	if len(df) > 0:
		if 'local_datetime' in df.columns:
			return df.set_index('local_datetime')['demand']

def gen(d):
	# Half Hourly Outturn Generation by Fuel Type
	# (this doesn't break out solar?)
	client = api.Client('ifkwpd4bwpopcle')
	print(d)
	df = client.get_FUELHH(d, (pd.to_datetime(d) + pd.Timedelta('1 day')).strftime('%Y-%m-%d'))
	if len(df) > 0:
		if 'local_datetime' in df.columns:
			return df.set_index('local_datetime').iloc[:, 3:-1]


# Other methods to obtain solar generation -- but these are the slow kinds
#get_B1440        Generation forecasts for Wind and Solar
#get_B1620        Actual Aggregated Generation per Type


df = client.get_B1440(d, (pd.to_datetime(d) + pd.Timedelta('1 day')).strftime('%Y-%m-%d'))
