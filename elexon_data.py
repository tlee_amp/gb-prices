#https://github.com/OSUKED/ElexonDataPortal/blob/master/ElexonDataPortal/api.py
import multiprocess as mp
import time
import pandas as pd
import sys
sys.path.append('D:/amp/europe/')  # Used to import elexon_helper
import elexon_helper
from functools import partial

FOLDER = 'D:/amp/europe/elexon'


# Also need spot gas prices https://www.erce.energy/graph/uk-natural-gas-nbp-spot-price/


dts_all = list(pd.date_range('2018-1-1','2022-3-20',freq='D').strftime('%Y-%m-%d'))

with mp.Pool(32) as p:
	demands = p.map(elexon_helper.dem, dts_all)
####pd.concat(demands).astype(float).to_csv('d:/amp/europe/elexon/demand.csv')
demands = pd.concat(demands).astype(float)
demands.index = pd.to_datetime(demands.index, utc=True).tz_localize(None)
demands = demands.groupby(level=0).mean()

demands.to_csv(FOLDER + '/gb_demand.csv')##, index_col=0, parse_dates=True)




# Get aggregated generation by type
dts_all = list(pd.date_range('2019-1-1','2022-3-20',freq='D').strftime('%Y-%m-%d'))
with mp.Pool(32) as p:
	gens = p.map(elexon_helper.gen, dts_all)

gens = pd.concat(gens).astype(float)


# Compare to reanalysis year of 2018
dts = list(pd.date_range('2018-1-1','2019-1-1',freq='D').strftime('%Y-%m-%d'))

onshores = [
'WHILW-1','WHILW-2','CRYRW-1',
'CRYRW-2','CRYRW-3','ARCHW-1','HADHW-1','BLLA-1','BLLA-2',
'FAARW-1','FAARW-2','BRDUW-1','GORDW-1','GORDW-2','PAUHW-1','CAIRW-1','CAIRW-2',
]
offshores = [
'BEATO-1', 'BEATO-2', 'BEATO-3', 'BEATO-4', 'ABRBO-1', 'HYWDW-1', 'RREW-1', 'RRWW-1'
]
codes = onshores + offshores
# Use parallel processing to download wind datasets 
for c in codes:
	print('NEW WIND FARM: ', c)
	inputs = dts
	t0 = time.time()
	with mp.Pool(32) as p:
	    out = p.map(partial(elexon_helper.f, code=c), inputs)
	t1 = time.time()
	print(t1 - t0)
	if len([x for x in out if x is not None]) > 0:
		pd.concat(out).to_csv(FOLDER + '/{}.csv'.format(c))


